package net.cydhra.vibrant.hooks;

import net.minecraft.client.Minecraft;

public aspect GuiFpsLockPatcher {

    public pointcut getFpsLimit():
            call(int net.minecraft.client.Minecraft.getLimitFramerate());

    int around(): getFpsLimit() {
        return Minecraft.getMinecraft().currentScreen != null ? 60 : proceed();
    }
}
