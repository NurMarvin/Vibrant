package net.cydhra.vibrant.hooks;

import net.cydhra.vibrant.api.entity.VibrantEntityLiving;
import net.cydhra.vibrant.aspects.AbstractRenderModelHook;

public aspect RenderModelHook extends AbstractRenderModelHook {

    @Override
    public pointcut renderModel(VibrantEntityLiving entity, float a, float b, float c, float d, float e, float f):
            call(void net.minecraft.client.renderer.entity.RendererLivingEntity.renderModel(Object, float, float, float, float, float, float))
                    && args(entity, a, b, c, d, e, f);
}
