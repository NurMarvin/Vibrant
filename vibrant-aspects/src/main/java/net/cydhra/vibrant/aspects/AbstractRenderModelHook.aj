package net.cydhra.vibrant.aspects;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.api.entity.VibrantEntityLiving;
import net.cydhra.vibrant.events.render.RenderModelEvent;

public abstract aspect AbstractRenderModelHook {

    pointcut renderModel(VibrantEntityLiving entity, float a, float b, float c, float d, float e, float f);

    before(VibrantEntityLiving entity, float a, float b, float c, float d, float e, float f): renderModel(entity, a, b, c, d, e, f) {
        EventManager.callEvent(new RenderModelEvent(entity));
    }
}
