package net.cydhra.vibrant.command.commands

import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * Echos the given argument string back into the callback.
 */
object EchoCommand : CommandHandler(true) {
    override fun executeCommand(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
        callback?.success(arguments.joinToString(" ")) ?: logger.error("Echo command invoked without callback function.")
    }

}