package net.cydhra.vibrant.command

import net.cydhra.vibrant.command.commands.EchoCommand
import net.cydhra.vibrant.command.commands.ToggleCommand
import net.cydhra.vibrant.command.commands.UserCommand
import org.cef.callback.CefQueryCallback

object CommandDispatcher {

    private val commandHandler = mutableMapOf<String, CommandHandler>()

    init {
        registerCommandHandler(UserCommand, "usercommand")
        registerCommandHandler(EchoCommand, "echo", "write", "print")
        registerCommandHandler(ToggleCommand, "toggle", "t")
    }

    fun registerCommandHandler(handler: CommandHandler, vararg commandAliases: String) {
        commandAliases.forEach { alias -> commandHandler[alias] = handler }
    }

    fun dispatchCommand(command: String, commandSender: CommandSender, callback: CefQueryCallback?) {
        if (command == "")
            return

        val arguments = command.split(" ")
        commandHandler[arguments[0]]
                ?.takeIf { commandSender != CommandSender.PLAYER || it.userCommand } // ignore client commands from user
                ?.executeCommand(origin = commandSender,
                        label = arguments[0],
                        arguments = *arguments.subList(1, arguments.size).toTypedArray(),
                        callback = callback
                ) ?: callback?.failure(-1, "${arguments[0]} is not a registered command alias")
        ?: throw IllegalArgumentException("${arguments[0]} is not a registered command alias")
    }
}