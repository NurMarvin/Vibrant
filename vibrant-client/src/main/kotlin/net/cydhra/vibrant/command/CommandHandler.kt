package net.cydhra.vibrant.command

import org.cef.callback.CefQueryCallback

@FunctionalInterface
abstract class CommandHandler(val userCommand: Boolean) {
    abstract fun executeCommand(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?)
}