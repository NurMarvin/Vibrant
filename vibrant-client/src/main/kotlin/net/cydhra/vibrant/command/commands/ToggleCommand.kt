package net.cydhra.vibrant.command.commands

import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.modulesystem.ModuleManager
import org.cef.callback.CefQueryCallback

/**
 * Command to toggle a module
 */
object ToggleCommand : CommandHandler(userCommand = true) {

    override fun executeCommand(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
        if (arguments.isEmpty()) {
            callback?.success("Usage: $label <module list>")
            return
        }

        val moduleList = ModuleManager.modules
                .filter { arguments.map { it.toLowerCase().replace("-", "") }.contains(it.name.toLowerCase().replace("-", "")) }

        moduleList.forEach(Module::toggle)

        if (!moduleList.isEmpty())
            callback?.success("Toggled ${moduleList.joinToString(", ") { it.name }}")
        else
            callback?.failure(-1, "No module found.")
    }
}