package net.cydhra.vibrant.command.commands

import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * Re-dispatches commands with the user as [CommandSender]
 */
object UserCommand : CommandHandler(false) {
    override fun executeCommand(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
        CommandDispatcher.dispatchCommand(arguments.joinToString(" "), CommandSender.PLAYER, callback)
    }
}