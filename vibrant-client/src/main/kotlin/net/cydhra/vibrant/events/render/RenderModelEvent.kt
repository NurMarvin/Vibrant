package net.cydhra.vibrant.events.render

import net.cydhra.eventsystem.events.Event
import net.cydhra.vibrant.api.entity.VibrantEntityLiving

class RenderModelEvent(val entity: VibrantEntityLiving) : Event()