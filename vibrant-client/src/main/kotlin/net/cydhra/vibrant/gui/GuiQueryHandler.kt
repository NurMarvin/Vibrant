package net.cydhra.vibrant.gui

import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.gui.mainmenu.MainMenuScreen
import net.cydhra.vibrant.gui.terminal.TerminalScreen
import org.cef.CefClient
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.browser.CefMessageRouter
import org.cef.callback.CefQueryCallback
import org.cef.handler.CefMessageRouterHandlerAdapter

/**
 * Handles JS queries from the browser client by calling the previously registered GUI callbacks.
 */
object GuiQueryHandler {

    /**
     * Initialize the handler by registering all GUI modules containing the callbacks invoked on a query.
     */
    fun initialize() {
        registerGuiModule(MainMenuScreen)
        registerGuiModule(TerminalScreen)
    }

    /**
     * Initialize the Chromium [CefClient] by registering a [MessageRouterHandler] responsible for handling JS queries to Vibrant.
     *
     * @param client [CefClient] that shall receive the [MessageRouterHandler]
     */
    fun initBrowserClient(client: CefClient) {
        val msgRouter = CefMessageRouter.create(CefMessageRouter.CefMessageRouterConfig("vibrantQuery", "vibrantQueryCancel"))
        msgRouter.addHandler(MessageRouterHandler(), true)
        client.addMessageRouter(msgRouter)
    }

    /**
     * Register the given [VibrantGuiModule] obtaining the methods marked with [QueryMethod]
     *
     * @param module a [VibrantGuiModule] supposedly implemented as a Companion to a subclass of [AbstractVibrantGuiScreen]
     */
    private fun registerGuiModule(module: VibrantGuiModule) {
        module.javaClass.methods.forEach { method ->
            if (method.isAnnotationPresent(QueryMethod::class.java)) {
                val annotation = method.getAnnotation(QueryMethod::class.java)
                CommandDispatcher.registerCommandHandler(object : CommandHandler(annotation.userCommand) {
                    override fun executeCommand(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
                        try {
                            method.invoke(module, origin, label, arguments, callback)
                        } catch (e: IllegalArgumentException) {
                            logger.error("Wrong method descriptor registered as QueryMethod", e)
                        }
                    }
                }, annotation.name)
            }
        }
    }

    /**
     * Handles the incoming queries deferring them to the respective method (registered by a [VibrantGuiModule])
     */
    @Suppress("UNUSED_PARAMETER")
    fun handleQuery(browser: CefBrowser, request: String, persistent: Boolean, callback: CefQueryCallback?) {
        CommandDispatcher.dispatchCommand(request, CommandSender.BROWSER, callback)
    }

    class MessageRouterHandler : CefMessageRouterHandlerAdapter() {
        override fun onQuery(browser: CefBrowser, frame: CefFrame?, queryId: Long, request: String, persistent: Boolean, callback: CefQueryCallback?): Boolean {
            GuiQueryHandler.handleQuery(browser, request, persistent, callback)
            return true
        }
    }
}