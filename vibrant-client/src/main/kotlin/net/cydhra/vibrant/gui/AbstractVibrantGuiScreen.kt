package net.cydhra.vibrant.gui

import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.api.client.VibrantMinecraft
import net.cydhra.vibrant.api.gui.VibrantGuiController
import net.cydhra.vibrant.api.gui.VibrantGuiScreen
import org.cef.CefApp
import org.cef.CefClient
import org.cef.CefSettings
import org.cef.browser.CefBrowserOsrLwjgl
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import java.awt.Color
import java.net.URL


/**
 * This class extends [VibrantGuiScreen] to override all the properties that must be implemented but are initialized by the adapter class.
 */
abstract class AbstractVibrantGuiScreen(protected val controller: VibrantGuiController) : VibrantGuiScreen {

    companion object {
        val settings = CefSettings().also { it.windowless_rendering_enabled = true }
        val client: CefClient = CefApp.getInstance(settings).createClient().also(GuiQueryHandler::initBrowserClient)
    }

    override var height: Int = -1
    override var width: Int = -1

    private var isInitialized = false

    protected val mc: VibrantMinecraft
        get() = VibrantClient.minecraft

    lateinit var browser: CefBrowserOsrLwjgl

    abstract val interfaceUrl: URL

    constructor() : this(VibrantClient.factory.newGuiController())

    override fun initGui() {
        if (!isInitialized) {
            this.initGuiFirstTime()
            isInitialized = true
        }
    }

    /**
     * Called when the GUI is initialized (on open) for the first time
     */
    open fun initGuiFirstTime() {
        browser = CefBrowserOsrLwjgl(client, this.interfaceUrl.toExternalForm(), true, null)
        browser.resize(mc.displayWidth, mc.displayHeight)
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        browser.mcefUpdate()
        VibrantClient.glStateManager.color(Color.WHITE)
        browser.draw(0.0, height.toDouble(), width.toDouble(), 0.0)
    }

    override fun tickScreen() {

    }

    override fun keyTyped(typedChar: Char, keyCode: Int) {
        browser.injectKeyTyped(typedChar, 0)
        super.keyTyped(typedChar, keyCode)
    }

    override fun onResize(mcIn: VibrantMinecraft, width: Int, height: Int) {
        browser.resize(mc.displayWidth, mc.displayHeight)
    }

    override fun handleInput() {
        while (Keyboard.next()) {
            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                escapeAction()
            }

            val pressed = Keyboard.getEventKeyState()
            val key = Keyboard.getEventCharacter()

            if (pressed)
                browser.injectKeyPressed(key, 0)
            else
                browser.injectKeyReleased(key, 0)

            if (key.toInt() != Keyboard.CHAR_NONE)
                browser.injectKeyTyped(key, 0)
        }

        while (Mouse.next()) {
            val btn = Mouse.getEventButton();
            val pressed = Mouse.getEventButtonState();
            val sx = Mouse.getEventX();
            val sy = Mouse.getEventY();

            val y = mc.displayHeight - sy

            if (btn == -1)
                browser.injectMouseMove(sx, y, 0, y < 0)
            else
                browser.injectMouseButton(sx, y, 0, btn + 1, pressed, 1)
        }
    }

    open fun escapeAction() {
        VibrantClient.minecraft.displayGuiScreen(null)
    }
}