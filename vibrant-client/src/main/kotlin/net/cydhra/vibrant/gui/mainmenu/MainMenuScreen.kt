package net.cydhra.vibrant.gui.mainmenu

import net.cydhra.vibrant.api.gui.VibrantGuiController
import net.cydhra.vibrant.api.gui.VibrantGuiMainMenu
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.gui.AbstractVibrantGuiScreen
import net.cydhra.vibrant.gui.QueryMethod
import net.cydhra.vibrant.gui.VibrantGuiModule
import org.cef.callback.CefQueryCallback
import java.net.URL

@Suppress("UNUSED_PARAMETER", "unused")
class MainMenuScreen(realMainMenu: VibrantGuiMainMenu) : AbstractVibrantGuiScreen(realMainMenu) {

    init {
        currentController = this.controller
    }

    override val interfaceUrl: URL = AbstractVibrantGuiScreen::class.java.getResource("/web/menu/MainMenu.html")

    companion object : VibrantGuiModule("mainmenu") {

        const val ACTION_SELECT_OPTIONS_SCREEN = 0
        const val ACTION_SELECT_WORLD_SCREEN = 1
        const val ACTION_MULTIPLAYER_SCREEN = 2
        const val ACTION_QUIT_GAME = 4
        const val ACTION_LANGUAGE_SCREEN = 5

        lateinit var currentController: VibrantGuiController

        @QueryMethod("singleplayer")
        fun displaySingleplayer(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
            currentController.actionPerformed(ACTION_SELECT_WORLD_SCREEN)
        }

        @QueryMethod("multiplayer")
        fun displayMultiplayer(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
            currentController.actionPerformed(ACTION_MULTIPLAYER_SCREEN)
        }

        @QueryMethod("options")
        fun displayOptions(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
            currentController.actionPerformed(ACTION_SELECT_OPTIONS_SCREEN)
        }

        @QueryMethod("selectLanguage")
        fun displayLanguages(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
            currentController.actionPerformed(ACTION_LANGUAGE_SCREEN)
        }

        @QueryMethod("quit")
        fun quitGame(origin: CommandSender, label: String, vararg arguments: String, callback: CefQueryCallback?) {
            currentController.actionPerformed(ACTION_QUIT_GAME)
        }
    }
}