package net.cydhra.vibrant.gui.terminal

import net.cydhra.vibrant.gui.AbstractVibrantGuiScreen
import net.cydhra.vibrant.gui.VibrantGuiModule
import java.net.URL

class TerminalScreen() : AbstractVibrantGuiScreen() {
    override val interfaceUrl: URL = AbstractVibrantGuiScreen::class.java.getResource("/web/terminal/Terminal.html")

    companion object : VibrantGuiModule("terminal")

}